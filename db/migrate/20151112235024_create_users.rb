class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :username
      t.string :email
      t.string :password_hash
      t.string :password_salt
      t.string :phone
      t.string :date_of_birth
      t.string :address
      t.string :city
      t.string :postal_code
      t.string :province
      t.string :country
      t.timestamps null: false
    end
  end
end
