class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.string :number
      t.string :name
      t.string :phone
      t.string :address
      t.string :email
      t.string :city
      t.string :postal_code
      t.string :province
      t.string :country
      t.string :owner_name
      t.string :owner_phone
      t.string :owner_address
      t.string :owner_email
      t.string :owner_city
      t.string :owner_postal_code
      t.string :owner_province
      t.string :owner_country
      t.string :username
      t.string :password_hash
      t.string :password_salt

      t.timestamps null: false
    end
  end
end
