class Store < ActiveRecord::Base
  attr_accessor :password
  before_save :encrypt_password, :generate_api_key

  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :username, :email
  validates_uniqueness_of :email, :username

  has_many :transactions

  def self.authenticate (username,password)
    store = find_by_username(username)
    if store && password_matches?(store,password)
      store
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def generate_api_key
    if !self.api_key
      self.api_key = Digest::SHA1.hexdigest([Time.now, rand].join)
    end
  end

  private
  def self.password_matches?(store,password_supplied)
     store.password_hash == BCrypt::Engine.hash_secret(password_supplied, store.password_salt)
  end
end
