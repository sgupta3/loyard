class User < ActiveRecord::Base
    attr_accessor :password, :fullname
    before_save :encrypt_password, :generate_loyard_id

    validates_confirmation_of :password
    validates_presence_of :password, :on => :create
    validates_presence_of :first_name, :last_name, :username, :email
    validates_uniqueness_of :email, :username

    has_many :transactions

    def self.authenticate (username,password)
      user = find_by_username(username)
      if user && password_matches?(user,password)
        user
      end
    end

    def fullname
      "#{first_name} #{last_name}"
    end

    def encrypt_password
      if password.present?
        self.password_salt = BCrypt::Engine.generate_salt
        self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
      end
    end

    def generate_loyard_id
      if !self.loyard_id
        self.loyard_id = Time.now.year.to_s + self.id.to_s + self.first_name[0] + Time.now.day.to_s + self.last_name[0] +  Random.rand(11).to_s
      end
    end

    private
    def self.password_matches?(user,password_supplied)
       user.password_hash == BCrypt::Engine.hash_secret(password_supplied, user.password_salt)
    end
end
