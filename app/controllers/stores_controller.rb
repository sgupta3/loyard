class StoresController < ApplicationController
  before_action :require_store_login, :except => [:new, :create]
  def new
    @store = Store.new
  end

  def create
      @store = Store.new(store_params)
      if @store.save
        redirect_to root_url, :flash => { :success => t("success_message") }
      else
        render 'new'
      end
  end

  def show
    @store = current_store
  end

  def edit
    @store = current_store
  end

  def update
    @store = current_store
    if @store.update(edit_store_params)
      flash.now[:success] = t('update_successful')
    else
      flash.now[:info] = t('update_unsuccessful')
    end
    render 'edit'
  end

  private
  def store_params
    params[:store].permit(:number, :name, :phone, :address, :email, :city, :postal_code, :province, :country, :owner_name, :owner_phone, :owner_address, :owner_email, :owner_city, :owner_postal_code, :owner_province, :owner_country, :username, :password, :password_confirmation )
  end

  def edit_store_params
    params[:store].permit(:number, :name, :phone, :address, :email, :city, :postal_code, :province, :country, :owner_name, :owner_phone, :owner_address, :owner_email, :owner_city, :owner_postal_code, :owner_province, :owner_country, :password, :password_confirmation )
  end
end
