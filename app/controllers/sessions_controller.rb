class SessionsController < ApplicationController
  layout "sans-nav"
  def new_user
  end

  def new_store
  end

  def authenticate_user
     user = User.authenticate(params[:username], params[:password])
     if user
       session[:store_id] = nil
       session[:user_id] = user.id
       redirect_to user_path(current_user)
     else
       flash.now[:danger] = t("login_error")
       render 'welcome/index'
     end
  end

  def authenticate_store
    store = Store.authenticate(params[:username], params[:password])
    if store
      session[:user_id] = nil
      session[:store_id] = store.id
      redirect_to store_path(current_store)
    else
      flash.now[:danger] = t("login_error")
      render 'welcome/index'
    end
  end

  def destroy_user
    if session[:user_id]
      session[:user_id] = nil
    end
    redirect_to root_url
  end

  def destroy_store
    if session[:store_id]
      session[:store_id] = nil
    end
    redirect_to root_url
  end
end
