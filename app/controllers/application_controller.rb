class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :current_user, :current_store
  before_action :set_locale


  def set_locale
    default_locale = :en
    I18n.locale = session[:locale] || default_locale
  end

  def change_locale
    locale = params[:locale]
    if (locale == "en" || locale == "fr")
      session[:locale] = locale
      render json: { message: 'successfully set the locale'}, :status => 200
    else
      render json: { error: 'error occured'}, :status => 400
    end
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def current_store
    @current_store ||= Store.find(session[:store_id]) if session[:store_id]
  end

  def require_user_login
    if !current_user
      redirect_to root_url
    end
  end

  def require_store_login
    if !current_store
      redirect_to root_url
    end
  end
end
