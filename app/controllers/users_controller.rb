class UsersController < ApplicationController
  before_action :require_user_login, :except => [:new, :create]
  def new
    @user = User.new
  end

  def create
      @user = User.new(user_params)
      if @user.save
        redirect_to root_url, :flash => { :success => t('success_message') }
      else
        render 'new'
      end
  end

  def show
    @user = current_user
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(edit_user_params)
      flash.now[:success] = t('update_successful')
    else
      flash.now[:info] = t('update_unsuccessful')
    end
    render 'edit'
  end


  private
  def user_params
    params[:user].permit(:first_name, :last_name, :username, :password, :password_confirmation, :email, :phone, :date_of_birth, :address, :city, :postal_code, :province, :country )
  end

  def edit_user_params
    params[:user].permit(:first_name, :last_name, :password, :password_confirmation, :email, :phone, :date_of_birth, :address, :city, :postal_code, :province, :country )
  end
end
