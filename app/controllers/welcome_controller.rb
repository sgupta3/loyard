class WelcomeController < ApplicationController
  layout "sans-nav"
  def index
    if current_user
      redirect_to user_path(current_user)
    elsif current_store
      redirect_to store_path(current_store)
    end
  end
end
