class TransactionsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  def new
    store = Store.find_by_api_key(params[:api_key])
    @user = User.find_by_loyard_id(params[:loyard_id])

    if (store && @user)
      transaction = @user.transactions.new
      transaction.store = store
      headers['Access-Control-Allow-Origin'] = '*'
        if transaction.save
          render :json => @user.as_json(:only => [:first_name, :last_name, :email, :phone, :date_of_birth, :address, :city, :postal_code, :province, :country])
        end
      else
        render json: { error: 'error occured'}, :status => 400
    end
  end

  private
  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, Accept, Authorization, Token'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  def cors_preflight_check
    if request.method == 'OPTIONS'
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
      headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-Prototype-Version, Token'
      headers['Access-Control-Max-Age'] = '1728000'

      render :text => '', :content_type => 'text/plain'
    end
  end
end
