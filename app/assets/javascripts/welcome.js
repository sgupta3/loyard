var ready;

ready = function() {
  if (document.getElementById( 'tabs' )) {
    new CBPFWTabs( document.getElementById( 'tabs' ) );
  }

  $(".change_locale").click(function(){
    locale = $(this).data('locale');
        $.ajax({
             type: "post",
             url: "/changelocale",
             dataType: "json",
             data: {
               "locale": locale
             },
             success: function(data) {
               location.reload();
             }
        });
  });
}

$(document).ready(ready);
$(document).on('page:load', ready);
