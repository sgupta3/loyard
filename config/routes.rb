Rails.application.routes.draw do
  
  root 'welcome#index'
  resources :users
  resources :stores

  match '/authenticate_user', :to => 'sessions#authenticate_user', :via => [:post], :as => :authenticate_user
  match '/authenticate_store', :to => 'sessions#authenticate_store', :via => [:post], :as => :authenticate_store

  get '/logout_user' => 'sessions#destroy_user', as: :logout_user
  get '/logout_store' => 'sessions#destroy_store', as: :logout_store

  match '/update_store', :to => 'stores#update', :via => [:patch], :as => :update_store
  match '/update_user', :to => 'users#update', :via => [:patch], :as => :update_user

  get '/api/loyard_id/:loyard_id/store/:api_key' => 'transactions#new', as: :new_transaction
  match '/changelocale', :to => 'application#change_locale', :via => [:post], :as => :changelocale

  get '/termsandconditions' => 'terms_and_conditions#show', as: :terms_and_conditions

end
